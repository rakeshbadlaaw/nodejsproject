angular.module('loginpage', [])
    .controller('login', ['$scope','$http', function($scope,$http) {
      $scope.user = {};
      $scope.submit= function(){
        console.log($scope.user);
       $http({
           url: '/processing',
           method: 'POST',
           data: $scope.user
       }).then(function (httpResponse) {
           console.log('response:', httpResponse.data);
           if(httpResponse.data=='true'){
             window.location.href ="/";
           }
       })
      }
    }]);
