var express = require('express');
var http =require('http');
var mysql = require('mysql');
var auth = require('node-session-tokens');
var bodyParser = require('body-parser');
var app = express();

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "mydb"
});
// Binding express app to port 3000
app.listen(3000,function(){
    console.log('Node server running @ http://localhost:3000');
});
var rand = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

var token = function() {
    return rand() + rand(); // to make it longer
};
function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
};



app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/node_modules',  express.static(__dirname + '/node_modules'));

app.use('/style',  express.static(__dirname + '/style'));
app.use('/controller',  express.static(__dirname + '/controller'));
app.use('/partials',  express.static(__dirname + '/partials'));
app.use('/api',  express.static(__dirname + '/partials'));
;

app.get('/',function(req,res){
    res.sendFile('home.html',{'root': __dirname + '/templates'});
});
app.get('/login',function(req,res){
    res.sendFile('login.html',{'root': __dirname + '/templates'});
});
app.post('/processing', function(req, res, next) {

    var cope = req.body.username;
    con.connect(function(err) {
      console.log('request received:', req.body.password);
      console.log("Connected!");
      $connectionquery= "select password from user where username = ?";
      var query = con.query($connectionquery, cope, function (err, result) {
          var keys = Object.keys(result);
          var len = keys.length;

            if (err) {
                console.error(err);
                return res.send(err);

            } else if(len == 1 && result[0].password == req.body.password){
              var token_key = token();
              var exp = addMinutes(30);
              var token = {
                'username': req.body.password,
                'token_key':token_key,
                'exp': exp
              };
              console.log(token);
                return res.send('true');
            }
            else{
              return res.send('false');
            }
      });
    });

});
